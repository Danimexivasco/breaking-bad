import React, { useState, useEffect } from 'react';
import styled from '@emotion/styled';
import Frase from './components/Frase';

const Contenedor = styled.div`
  display:flex;
  align-items: center;
  padding-top: 5rem;
  flex-direction: column;
`;

const Boton = styled.button`
  background: -webkit-linear-gradient(top left, #007d35 0%, #007d35 40%, #0f574e 100%);
  background-size: 305px;
  font-family: Arial, Helvetica, sans-serif;
  color: #fff;
  margin-top: 3rem;
  padding: 1rem 3rem;
  font-size: 2rem;
  border: 2px solid black;
  transition: border .5s ease;

  &:hover{
    cursor:pointer;
    border: 2px solid white;
  }
`;


function App() {

  // State de la frase
  const [frase, guardarFrase] = useState({});

  const consultarAPI = async () => {
    const api = await fetch('http://breaking-bad-quotes.herokuapp.com/v1/quotes')
    const frase = await api.json()
    guardarFrase(frase[0])

    // Sin async/await
    // const frase = api.then(respuesta =>{
    //   return respuesta.json;
    // })
    // frase.then(resultado=> console.log(resultado))
  }

  // Cargar una frase cuando la página carge
  useEffect(() => {
    consultarAPI()
  }, [])

  return (
    <Contenedor>
      <Frase
        frase={frase}
      />
      <Boton
        onClick={consultarAPI}
      > Generar Frase</Boton>
    </Contenedor>

  );
}

export default App;
